const Joi = require('@hapi/joi');


function validate(schema, body, res, next) {
    const { error, value } = schema.validate(body);
    if (error) {
        console.log(error);
        return res.status(400).json({ error: true, result: error.message });
    }
    next();
}


function add(req, res, next) {
    const schema = Joi.object().keys({
        type: Joi.string().valid('cycling', 'running', 'walking').required(),
        location: Joi.object().keys({
            lat: Joi.number().required(),
            lon: Joi.number().required(),
        }).required(),
        duration: Joi.number().required(),
        comments: Joi.string(),
    });
    validate(schema, req.body, res, next);
}

function update(req, res, next) {
    const schema = Joi.object().keys({
        type: Joi.string().valid('cycling', 'running', 'walking'),
        location: Joi.object().keys({
            lat: Joi.number().required(),
            lon: Joi.number().required(),
        }),
        duration: Joi.number(),
        comments: Joi.string(),
    });
    validate(schema, req.body, res, next);
}

function get(req, res, next) {
    const schema = Joi.object().keys({
        minDate: Joi.date(),
        maxDate: Joi.date(),
        minPolution: Joi.number(),
        maxPolution: Joi.number(),
    });
    validate(schema, req.query, res, next);
}

module.exports = {
    add,
    update,
    get,
}