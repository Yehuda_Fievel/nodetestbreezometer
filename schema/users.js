const Joi = require('@hapi/joi');


function validate(schema, body, res, next) {
    const { error, value } = schema.validate(body);
    if (error) {
        console.log(error);
        return res.status(400).json({ error: true, result: error.message });
    }
    next();
}


function register(req, res, next) {
    const schema = Joi.object().keys({
        password: Joi.string().required(),
        name: Joi.string().required(),
        age: Joi.number(),
        city: Joi.string(),
    });
    validate(schema, req.body, res, next);
}


module.exports = {
    register,
}