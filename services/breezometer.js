const breezometer = require('node-breezometer');
const config = require('../config')
 
const client = breezometer({ apiKey: config.breezometerAPIKey });


function getAirQuality({lat, lon}) {
    return client.getAirQuality({ lat, lon });
}


module.exports = {
    getAirQuality,
}