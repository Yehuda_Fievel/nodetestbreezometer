const express = require('express');
const router = express.Router();
const workoutSchema = require('../schema/workouts');
const workoutCtrl = require('../controllers/workouts');


router.post('', workoutSchema.add, workoutCtrl.add);

router.put('/:id', workoutSchema.update, workoutCtrl.isUserWorkout, workoutCtrl.update);

router.delete('/:id', workoutCtrl.isUserWorkout, workoutCtrl.del);

router.get('', workoutSchema.get, workoutCtrl.get);


module.exports = router;