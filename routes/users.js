const express = require('express');
const router = express.Router();
const userSchema = require('../schema/users');
const userCtrl = require('../controllers/users');


router.post('/register', userSchema.register, userCtrl.register);


module.exports = router;