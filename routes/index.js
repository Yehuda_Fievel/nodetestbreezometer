const passport = require('passport');
const { BasicStrategy } = require('passport-http');
const userCtrl = require('../controllers/users');

module.exports = function (app) {
    app.use('/users', require('./users'));
    app.use('/workouts', authenticateUser, require('./workouts'));


    // Error Handler
    app.use((err, req, res, next) => {
        console.log(err);
        res.status(err.statusCode || 500).json({ error: true, message: err.message });
    });

    function authenticateUser(req, res, next) {
        passport.authenticate('basic', { session: false })(req, res, next);
    }
}

passport.use(new BasicStrategy(
    function (name, password, done) {
        userCtrl.findOne({ name }, (err, user) => {
            if (err) { return done(err); }
            if (!user) { return done(null, false); }
            if (!user.verifyPasswordSync(password)) { return done(null, false); }
            return done(null, user);
        });
    }
));