const mongoose = require('mongoose');
const bcrypt = require('mongoose-bcrypt');


const Users = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    age: {
        type: Number,
    },
    city: {
        type: String,
    },
}, { timestamps: true });

Users.plugin(bcrypt);


module.exports = exports = mongoose.model('User', Users, 'users')