const mongoose = require('mongoose');


const Workouts = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        index: true,
    },
    type: {
        type: String,
        enum: ['cycling', 'running', 'walking'],
        required: true,
    },
    location: {
        type: {
            lat: Number,
            lon: Number,
        },
        required: true,
    },
    date: {
        type: String,
        required: true,
    },
    time: {
        type: String,
        required: true,
    },
    duration: {
        type: String,
        required: true,
    },
    comments: {
        type: String,
    },
    pollutionLevel: {
        type: Number,
        required: true,
    }
}, { timestamps: true });


module.exports = exports = mongoose.model('Workouts', Workouts, 'workouts')