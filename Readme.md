# Node.js test using Breezometer


## Justify 3rd-party packages

**@hapi/joi** - used for validating user request
**body-parser** - used for express to read post body
**moment** - used to help parse date object to date and time format
**mongoose** - used to connect to mongoDB and create model schema
**mongoose-bcrypt** - used to add bcrypt password in Mongoose
**passport-http** - used for basic auth
**nodemon** - used for easy restart during dev testing