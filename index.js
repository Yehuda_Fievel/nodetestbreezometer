const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const config = require('./config')
const port = process.env.PORT || config.port;


app.use(bodyParser.json());

require('./db');

require('./routes')(app);

app.listen(port, () => console.log(`App is listening on port ${port}!`));