const Workout = require('../models/workouts');
const breezometer = require('../services/breezometer');
const moment = require('moment');


async function add(req, res, next) {
    try {
        const aqi = await breezometer.getAirQuality(req.body.location);
        req.body.user = req.user;
        req.body.pollutionLevel = aqi.data.indexes.baqi.aqi;
        req.body.date = moment().format('l');
        req.body.time = moment().format('HH:mm');
        const workout = await Workout.create(req.body);
        res.status(201).json({ response: workout });
    } catch (error) {
        next(error);
    }
}

async function update(req, res, next) {
    try {
        if (req.workout.location.lat != req.body.location.lat || req.workout.location.lon != req.body.location.lon) {
            const aqi = await breezometer.getAirQuality(req.body.location);
            req.workout.pollutionLevel = aqi.data.indexes.baqi.aqi;
        }
        for (let index in req.body) {
            req.workout[index] = req.body[index];
        }
        await req.workout.save();
        res.status(200).json({ response: req.workout });
    } catch (error) {
        next(error);
    }
}

async function del(req, res, next) {
    try {
        const workout = await Workout.deleteOne({ _id: req.params.id });
        res.status(200).json({ response: 'workout deleted' });
    } catch (error) {
        next(error);
    }
}

async function get(req, res, next) {
    try {
        const query = { user: req.user };
        if (req.query.minDate && req.query.maxDate) {
            query.createdAt = {
                $gte: moment.utc(req.query.minDate).format(),
                $lt: moment.utc(req.query.maxDate).format(),
            }
        }
        if (req.query.minPolution && req.query.maxPolution) {
            query.pollutionLevel = {
                $gte: req.query.minPolution,
                $lt: req.query.maxPolution,
            }
        }
        const workouts = await Workout.find(query);
        res.status(200).json({ response: workouts });
    } catch (error) {
        next(error);
    }
}


async function isUserWorkout(req, res, next) {
    try {
        req.workout = await Workout.findOne({ _id: req.params.id, user: req.user });
        if (!req.workout) {
            return res.status(404).json({ message: 'Did not find workout' });
        }
        next();
    } catch (error) {
        next(error);
    }
}

module.exports = {
    add,
    update,
    del,
    get,
    isUserWorkout
}