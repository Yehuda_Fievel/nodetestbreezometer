const User = require('../models/users');


async function register(req, res, next) {
    try {
        const user = await User.create(req.body);
        res.status(201).json({ message: 'User created' });
    } catch (error) {
        next(error);
    }
}

async function findOne(name, callback) {
    User.findOne(name).exec(callback);
}

module.exports = {
    register,
    findOne,
}